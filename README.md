# Monitoring App

A simple Python script to monitor your computer and check your listening ports

## prerequisites

[Download Python](https://www.python.org/downloads/)
(get the last version to avoid any issues)


## installation

- clone the repository:

```
git clone https://gitlab.com/ynov18/monitoring.git
cd monitoring
```

- install dependencies :  

```
pip install psutil
```

## Usage

- Check System Resources

Execute the following command to generate a report on system resources:

```
python monit.py check
```

This  will create a log file here -> var/monit/ .

- List Reports

List all available reports:

```
python monit.py list
```

- Get Last Report

This will give you the last generated report:

```
python monit.py get last
```

- Get Average Report (not working well now :/  )
 
Retrieve and display the average report for the last X hours (replace X with the desired number of hours):

```
python monit.py get avg X
```

## Configuration

Edit the config.json file to specify the ports you want to monitor :

```
{
  "ports_to_check": [80, 443, 22]
}

```